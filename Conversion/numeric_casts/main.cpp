#include <iostream>

#include <boost/cast.hpp>

using namespace std;

int main()
{
    int x = 8999;

    short sx = boost::numeric_cast<short>(x);

    cout << "x = " << x << endl;
    cout << "sx = " << sx << endl;

    x = 31;

    unsigned int ux = boost::numeric_cast<unsigned int>(x);

    cout << "ux = " << ux << endl;
}

