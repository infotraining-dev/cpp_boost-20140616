#include "Deleter.hpp"
#include "ToBeDeleted.hpp"
#include <vector>

class SomeClass;

SomeClass* create()
{
	return (SomeClass*)0;
}

void simple_test()
{
//    SomeClass* p = create();

//    delete p; // w najlepszym razie warning
}

void real_test()
{
	ToBeDeleted* tbd = new ToBeDeleted();

	Deleter exterminator;
	exterminator.delete_it(tbd);
}

int main()
{
    std::vector<ToBeDeleted*> vec;

    vec.push_back(new ToBeDeleted);
    vec.push_back(new ToBeDeleted);
    vec.push_back(new ToBeDeleted);
    vec.push_back(new ToBeDeleted);

    std::for_each(vec.begin(), vec.end(),
                  &boost::checked_delete<ToBeDeleted>);

//    for(std::vector<ToBeDeleted*>::iterator it = vec.begin();
//        it != vec.end(); ++it)
//    {
//        delete *it;
//    }


	real_test();
}
