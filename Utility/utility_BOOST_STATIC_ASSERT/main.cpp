#include <iostream>
#include <boost/static_assert.hpp>
#include <boost/type_traits/integral_constant.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

class Base {};

class Derived : public Base {};

class Different {};

template <typename T>
void delete_it(T* arg)
{
    static_assert(sizeof(T) > 0, "Nie mozna wywolywac delete dla typu niekompletnego");

    delete arg;
}

template <typename T>
class OnlyCompatibleWithIntegralTypes
{
    BOOST_STATIC_ASSERT(boost::is_integral<T>::value);
    static_assert(boost::is_integral<T>::value, "T must be integral");
};

template <typename T, size_t i>
void accepts_arrays_with_size_between_1_and_100(T (&arr)[i])
{
	BOOST_STATIC_ASSERT(i>=1 && i<=100);
}

void expects_ints_to_be_4_bytes()
{
	BOOST_STATIC_ASSERT(sizeof(int)==4);
}

template <typename T>
void works_with_base_and_derived(T& t)
{
	BOOST_STATIC_ASSERT((boost::is_base_of<Base, T>::value));
}

template <typename T>
struct is_auto_ptr : public boost::false_type
{
};

template <typename T>
struct is_auto_ptr<std::auto_ptr<T>> : public boost::true_type
{
};

template <typename T>
void works_with_pointers(T ptr)
{
    //BOOST_STATIC_ASSERT(is_auto_ptr<T>::value == false);
    static_assert(!is_auto_ptr<T>::value, "Nie mozna przekazac auto_ptr jako argument");

    std::cout << "Value: " << *ptr << std::endl;
}


class Unknown;

int main()
{
//    Unknown* u = NULL;
//    delete_it(u);

    OnlyCompatibleWithIntegralTypes<int> test1;

    int arr[50];

	accepts_arrays_with_size_between_1_and_100(arr);

    Derived arg;
	works_with_base_and_derived(arg);

    int* ptr = new int(10);

    works_with_pointers(ptr); // ok

    boost::shared_ptr<int> sptr = boost::make_shared<int>(10);

    works_with_pointers(ptr); // ok

    std::auto_ptr<int> aptr(new int(10));

    works_with_pointers(aptr); // error

}
