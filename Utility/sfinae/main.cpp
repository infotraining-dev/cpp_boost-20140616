#include <iostream>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_integral.hpp>

using namespace std;

void foo(int arg)
{
    cout << "foo(int: " << arg << ")" << endl;
}

struct Short
{
    typedef void nested_type;

    short value;
};

ostream& operator<<(ostream& out, const Short& s)
{
    out << s.value;
    return out;
}

// 1 - sposob
template <typename T>
typename boost::disable_if<boost::is_integral<T> >::type foo(T arg)
{
    typename T::nested_type* val;

    cout << "foo(T: " << arg << ")" << endl;
}

// 2 - sposob
template <typename T>
void foo(T arg, typename boost::disable_if<boost::is_integral<T> >::type* ptr = 0)
{
    typename T::nested_type* val;

    cout << "foo(T: " << arg << ")" << endl;
}

int main()
{
    foo(1);

    int x = 10;
    foo(x);

    short sx = 20;
    foo(sx);

//    Short sht { 30 };

//    foo(sht);
}

