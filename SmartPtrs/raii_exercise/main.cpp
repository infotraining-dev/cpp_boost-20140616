#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

const char* get_line()
{
	static size_t count = 0;

	if (++count == 13)
		throw std::runtime_error("Blad!!!");

	return "Hello RAII\n";
}

class FileGuard : boost::noncopyable
{
    FILE* file_;
public:
    FileGuard(FILE* file) : file_(file)
    {
        if (file_ == 0)
            throw std::runtime_error("Blad otwarcia pliku!!!");
    }

    FileGuard(const char* filename, const char* mode)
    {
        file_ = fopen(filename, mode);

        if (file_ == 0)
            throw std::runtime_error("Blad otwarcia pliku!!!");
    }

    ~FileGuard()
    {
        fclose(file_);
    }

    FILE* get() const
    {
        return file_;
    }
};

void save_to_file(const char* file_name)
{
	FILE* file = fopen(file_name, "w");

	if ( file == 0 )
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for(size_t i = 0; i < 100; ++i)
		fprintf(file, get_line());

	fclose(file);
}


// TO DO: RAII
void save_to_file_with_raii(const char* file_name)
{
    FileGuard file(fopen(file_name, "w"));

    //FileGuard copy_of_file = file;

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

void cleanup(void*)
{
    std::cout << "cleanup" << std::endl;
}

void save_to_file_with_sp(const char* file_name)
{
    FILE* file = fopen(file_name, "w");

    if ( file == 0 )
        throw std::runtime_error("Blad otwarcia pliku!!!");

    boost::shared_ptr<FILE> sp_file(file, &fclose);

    for(size_t i = 0; i < 100; ++i)
        fprintf(sp_file.get(), get_line());
}

void save_to_file_with_up(const char* file_name)
{
    std::unique_ptr<FILE, int(*)(FILE*)>
            file(fopen(file_name, "w"), &fclose);

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

int main()
try
{
    //save_to_file("text.txt");
    //save_to_file_with_raii("text.txt");
    save_to_file_with_sp("text.txt");
}
catch(const std::exception& e)
{
	std::cout << e.what() << std::endl;

    std::string temp;
    std::getline(std::cin, temp);
}
