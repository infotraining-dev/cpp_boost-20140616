#include <iostream>
#include <stdexcept>
#include <memory>
#include <cassert>
#include <vector>

using namespace std;

class ManagedObject
{
    int id_;
public:
    ManagedObject(int id = 0) : id_(id)
    {
        cout << "ManagedObject(" << id_ << ")" << endl;
    }

    ~ManagedObject()
    {
        cout << "~ManagedObject(" << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }
};

void may_throw(int arg)
{
    cout << "may_throw(" << arg << ")" << endl;

    if (arg == 13)
        throw runtime_error("Error#13");
}

auto_ptr<ManagedObject> create(int id)
{
    return auto_ptr<ManagedObject>(new ManagedObject(id));
}

void sink(unique_ptr<ManagedObject> arg)
{
    cout << "sink function: ID = " << arg->id() << endl;
}

int main() try
{
    {
        {
            unique_ptr<ManagedObject[]> uarr(new ManagedObject[10]);

            cout << "ID: " << uarr[3].id() << endl;
        }

        {
            vector<unique_ptr<ManagedObject>> vec;

            unique_ptr<ManagedObject> uptr1(new ManagedObject(7));

            vec.push_back(move(uptr1));
            vec.push_back(create(8));
            vec.push_back(create(9));

            for(int i = 0; i < vec.size(); ++i)
                cout << vec[i]->id() << endl;

            unique_ptr<ManagedObject> uptr2 = move(vec[1]);
            cout << "ID for uptr2: " << uptr2->id() << endl;
        }

        unique_ptr<ManagedObject> uptr1(new ManagedObject(1));

        cout << "ID: " << uptr1->id() << endl;

        ManagedObject* raw_ptr = uptr1.get();
        cout << "ID: " << raw_ptr->id() << endl;

        uptr1.reset(new ManagedObject(2));

        unique_ptr<ManagedObject> uptr2 = move(uptr1);

        assert(uptr1.get() == NULL);

        cout << "ID aptr2: " << uptr2->id() << endl;

        uptr2 = create(3);

        sink(move(uptr2));
        sink(create(4));

        may_throw(13);

        raw_ptr = uptr1.release();
    }
}
catch(const exception& e)
{
    cout << "Exception catched: " << e.what() << endl;
}

