#include <iostream>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <cassert>
#include <cstdlib>
#include <stdexcept>

class X
{
public:
    // konstruktor
    X(int value = 0)
        : value_(value)
    {
        std::cout << "Konstruktor X(" << value_ << ")\n";
    }

    // destruktor
    ~X()
    {
        std::cout << "Destruktor ~X(" << value_ << ")\n";
    }

    int value() const
    {
        return value_;
    }

    void set_value(int value)
    {
        value_ = value;
    }

    void unsafe()
    {
        throw std::runtime_error("ERROR");
    }

private:
    int value_;
};


class A
{
	boost::shared_ptr<X> x_;
public:
	A(boost::shared_ptr<X> x) : x_(x) {}
	
	int get_value()
	{
		return x_->value();
	}
};

class B
{
	boost::shared_ptr<X> x_;
public:
	B(boost::shared_ptr<X> x) : x_(x) {}
	
	void set_value(int i)
	{
        x_->set_value(i);
	}	
};

using namespace std;

class BaseManagedObject
{
    int id_;
public:
    BaseManagedObject(int id = 0) : id_(id)
    {
        cout << "ManagedObject(" << id_ << ")" << endl;
    }

    virtual ~BaseManagedObject()
    {
        cout << "~ManagedObject(" << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }
};

class Derived : public BaseManagedObject
{
    string value_;
public:
    Derived(int id, string val) : BaseManagedObject(id), value_(val)
    {
        cout << "Derived(" << id << ", " << val << ")" << endl;
    }

    ~Derived()
    {
        cout << "~Derived("  << id() << ", " << value_ << ")" << endl;
    }

    string value() const
    {
        return value_;
    }
};

int main()
{
    {
        //boost::shared_ptr<BaseManagedObject> base_ptr(new Derived(1, "Derived"));
        boost::shared_ptr<BaseManagedObject> base_ptr(new BaseManagedObject(1));

        boost::shared_ptr<Derived> derived_ptr
                = boost::dynamic_pointer_cast<Derived>(base_ptr);

        cout << "ID: " << base_ptr->id() << endl;

        if (derived_ptr)
            cout << "Value: " << derived_ptr->value() << endl;
    }

    std::cout << "\n//----------------------\n\n";

	{
        //boost::shared_ptr<X> spX1(new X(10));
        boost::shared_ptr<X> spX1 = boost::make_shared<X>(10);

		std::cout << "RC: " << spX1.use_count() << std::endl;
		{
			boost::shared_ptr<X> spX2(spX1);
			std::cout << "RC: " << spX2.use_count() << std::endl;

            boost::shared_ptr<X> spX3;

            if (!spX3)
            {
                spX3 = spX1;
                std::cout << "RC: " << spX1.use_count() << std::endl;
            }
		}
		std::cout << "RC: " << spX1.use_count() << std::endl;
	}

	std::cout << "\n//----------------------\n\n";

	boost::shared_ptr<X> temp( new X(14) );
	A a(temp);
	{
		B b(temp);
		b.set_value(28);
	}
	
	assert(a.get_value() == 28);

	std::cout << "a.get_value() = " << a.get_value() << std::endl;
}
