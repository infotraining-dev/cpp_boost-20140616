#include <iostream>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

using namespace std;

void function1(int x, double y)
{
    cout << "function1(" << x << ", " << y << ")" << endl;
}

class FunctorA : public std::binary_function<int, int, int>
{
public:
    int operator()(int x, int y)
    {
        return x + y;
    }
};

class FunctorB
{
    int call_number_ = 0;

public:
    //typedef void result_type;

    void operator()(int x, int y)
    {
        cout << x + y << endl;
        ++call_number_;
    }

    int number_of_calls() const
    {
        return call_number_;
    }
};

class Person
{
    string name_;
    int age_;
public:
    Person(const string& name, int age) : name_(name), age_(age)
    {}

    int age() const
    {
        return age_;
    }

    void print(const string& prefix) const
    {
        cout << prefix << " name=" << name_ << ", age=" << age_ << endl;
    }
};

void function2(ostream& out, const string& text, int& x)
{
    out << "function2(text = " << text << ", x = " << ++x << ")" << endl;
}

int main()
{
    auto f1 = boost::bind(&function1, 1, 2);

    f1();

    auto f2 = boost::bind(&function1, 2, _1);

    int x = 10;

    f2(x);
    f2(88);

    auto f3 = boost::bind(&function1, _2, _1);

    f3(1, 2); // function1(2, 1);

    FunctorA func1;

    auto f4 = boost::bind(func1, _1, 10);

    cout << f4(20) << endl;

    FunctorB func2;

    auto f5 = boost::bind<void>(boost::ref(func2), _1, 10);

    f5(19);
    f5(23);

    cout << "number of calls for func2: " << func2.number_of_calls() << endl;

    x = 0;

    auto f6 = boost::bind(&function2,
                          boost::ref(cout), "Hello Bind", boost::ref(x));

    f6();
    f6();
    f6();

    Person p("Kowalski", 22);
    boost::shared_ptr<Person> sp = boost::make_shared<Person>("Nowak", 55);

    auto f7 = boost::bind(&Person::print, boost::ref(p), "Osoba: ");
    auto f8 = boost::bind(&Person::print, &p, "Osoba: ");
    auto f9 = boost::bind(&Person::print, sp, "Osoba: ");

    f7();
    f8();
    f9();

    auto f10 = boost::bind(&Person::print, _1, "Osoba: ");

    f10(&p);
    f10(sp);

    cout << "\n\n";

    vector<Person> people = { p, *sp, Person("Anonim", 70),
                              Person("Nijaki", 44) };

    for_each(people.begin(), people.end(),
             boost::bind(&Person::print, _1, "Osoba: "));

    cout << "\n";

    auto where = find_if(people.begin(), people.end(),
                         !boost::bind(greater<int>(),
                                     boost::bind(&Person::age, _1),
                                     67));

    auto where = find_if(people.begin(), people.end(),
                         boost::bind(&Person::age, _1) > 67);


    if (where != people.end())
        where->print("Osoba: ");

}

