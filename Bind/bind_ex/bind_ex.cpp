#include "person.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <numeric>
#include <functional>
#include <boost/algorithm/cxx11/copy_if.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>

using namespace std;

template <typename InIt, typename OutIt, typename Pred>
void mcopy_if(InIt start, InIt end, OutIt dest, Pred pred)
{
    while (start != end)
    {
        if (pred(*start))
        {
            *dest = *start;
        }

        ++dest;
        ++start;
    }
}

class BoundSalary
{
    double salary_ = 3000.0;
public:
    bool operator()(const Person& p)
    {
        return p.salary() > salary_;
    }
};

int main()
{
    vector<Person> employees;
    fill_person_container(employees);

    cout << "Wszyscy pracownicy:\n";
    copy(employees.begin(), employees.end(),
         ostream_iterator<Person>(cout, "\n"));
    cout << endl;

    // wyświetl pracownikow z pensją powyżej 3000
    cout << "\nPracownicy z pensja powyżej 3000:\n";
    copy_if(employees.begin(), employees.end(),
            ostream_iterator<Person>(cout, "\n"),
            boost::bind(&Person::salary, _1) > 3000.0);

    // wyświetl pracowników o wieku poniżej 30 lat
    cout << "\nPracownicy o wieku poniżej 30 lat:\n";
    copy_if(employees.begin(), employees.end(),
            ostream_iterator<Person>(cout, "\n"),
            //boost::bind(&Person::age, _1) < 30);
            [](const Person& p) { return p.age() < 30; });


    // posortuj malejąco pracownikow wg nazwiska
    cout << "\nLista pracowników wg nazwiska (malejaco):\n";
    sort(employees.begin(), employees.end(),
         boost::bind(&Person::name, _1) > boost::bind(&Person::name, _2));

    copy(employees.begin(), employees.end(),
         ostream_iterator<Person>(cout, "\n"));

    // wyświetl kobiety
    cout << "\nKobiety:\n";
    copy_if(employees.begin(), employees.end(),
            ostream_iterator<Person>(cout, "\n"),
            boost::bind(&Person::gender, _1) == Female);

    // ilość osob zarabiajacych powyżej średniej

    boost::function<double(double, Person)> salary_reducer = boost::bind(
        plus<double>(),
            _1,
            boost::bind(&Person::salary, _2));

    double avg = accumulate(employees.begin(), employees.end(), 0.0,
                            salary_reducer) / employees.size();

    cout << "\nAVG: " << avg << endl;
    cout << "\nIlosc osob zarabiajacych powyzej sredniej:\n";
    cout << count_if(employees.begin(), employees.end(),
                     boost::bind(&Person::salary, _1) > avg) << endl;
}
