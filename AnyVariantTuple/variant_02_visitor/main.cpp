#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>
#include <boost/variant.hpp>

using namespace std;

namespace std
{
    template <typename T1, typename T2>
    ostream& operator<<(ostream& out, const pair<T1, T2>& p)
    {
        out << "(" << p.first << ", " << p.second <<")";
        return out;
    }
}

template <typename Variant>
void times_two(Variant& v)
{
	if (int* pi = boost::get<int>(&v))
		*pi *= 2;
	else if (string* pstr = boost::get<string>(&v))
		*pstr += *pstr;
}

class TimesTwoVisitor :public boost::static_visitor<>
{
public:
	void operator()(int& i) const
	{
		i *= 2;
	}

	void operator()(std::string& str) const
	{
		str += str;
	}

	void operator()(std::complex<double>& c) const
	{
		c += c;
	}

    void operator()(pair<int, double>& p) const
    {
        p.first += p.first;
        p.second += p.second;
    }
};

int main()
{
    boost::variant<int, string, complex<double>, pair<int, double> > var;

	var = 5;
	times_two(var);
	cout << "var = " << var << endl;

	var = "Tekst...";
	times_two(var);
	cout << "var = " << var << endl;

    // to samo z wizytorem
	boost::apply_visitor(TimesTwoVisitor(), var);
	cout << "var = " << var << endl;

    // wizytacja opozniona
	vector<boost::variant<int, string> > vars;
	vars.push_back(1);
	vars.push_back("two");
	vars.push_back(3);
	vars.push_back("four");

	TimesTwoVisitor visitor;
	for_each(vars.begin(), vars.end(), boost::apply_visitor(visitor));

	cout << "vars: ";
	copy(vars.begin(), vars.end(), ostream_iterator<boost::variant<int, string> >(cout, " "));
	cout << endl;
}
