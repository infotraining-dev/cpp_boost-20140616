#include <iostream>
#include <string>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_io.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;

template <typename T>
void print_tuple(T& t, string prefix)
{
    cout << boost::tuples::set_open('{')
         << boost::tuples::set_close('}')
		 << boost::tuples::set_delimiter(',')
		 << prefix << " = " << t << endl;
}

boost::tuple<int, int> find_min_max(const vector<int>& vec)
{
    auto min = min_element(vec.begin(), vec.end());
    auto max = max_element(vec.begin(), vec.end());

    return boost::make_tuple(*min, *max);
}

template <typename Tuple, typename Func, size_t Index>
struct ForEachHelper
{
    static void call(Tuple t, Func f)
    {
        ForEachHelper<Tuple, Func, Index -1>::call(t, f);

        f(t.get<Index>());
    }
};

template <typename Tuple, typename Func>
struct ForEachHelper<Tuple, Func, 0>
{
    static void call(Tuple t, Func f)
    {
        f(t.get<0>());
    }
};

template <typename Tuple, typename Func>
void for_each(Tuple t, Func f)
{
    ForEachHelper<Tuple, Func, boost::tuples::length<Tuple>::value-1>::call(t, f);
}

class Printer
{
public:
    void operator()(int x) const
    {
        cout << "int: " << x << endl;
    }

    void operator()(double d) const
    {
        cout << "double: " << d << endl;
    }

    void operator()(const string& s) const
    {
        cout << "string: " << s << endl;
    }

};

int main()
{
    cout << boost::tuples::set_open('{')
         << boost::tuples::set_close('}')
         << boost::tuples::set_delimiter(',') << endl;

	// krotka
    typedef boost::tuple<int, double, string> Triple;

    cout << "Size of triple: " << boost::tuples::length<Triple>::value << endl;

    Triple triple(43, 3.1415, "Krotka...");

    for_each(triple, Printer());

	// odwolania do krotki
    cout << "triple = "
//         << triple.get<0>() << ", "
//		 << triple.get<1>() << ", "
//		 << boost::tuples::get<2>(triple) << ")" << endl;
         << triple << endl;


    // domyslna inicjalizacja krotki
	boost::tuple<short, bool, string> default_tuple;
	print_tuple(default_tuple, "default_tuple");

	// funkcja pomocnicza - make tuple
	triple = boost::make_tuple(12, 32.222, "Inna krotka...");
	print_tuple(triple, "triple");

	triple.get<2>() = "Inny tekst...";
	print_tuple(triple, "triple");

	// krotki z referencjami
	int x = 10;
	string str = "Tekst...";

    boost::tuple<int&, string&> ref_tpl(x, str);
	ref_tpl.get<0>()++;
	boost::to_upper(ref_tpl.get<1>());

	cout << "x = " << x << endl;
	cout << "str = " << str << endl;

    boost::tuple<const int&, string&> cref_tpl
            = boost::make_tuple(boost::cref(x), boost::ref(str));

	//cref_tpl.get<0>()++; // Blad! Referencja do stalej!
    boost::to_lower(cref_tpl.get<1>());

	cout << "x = " << x << endl;
	cout << "str = " << str << endl;

    int min;
    //int max;

    vector<int> numbers = { 7, 2, 1, 6, 0, 10, 3, 8, 2 };

//    boost::tuple<int&, int&> minmax(min, max);
//    minmax = find_min_max(numbers);

    boost::tie(min, boost::tuples::ignore) = find_min_max(numbers);

    cout << "min = " << min << endl;
    //cout << "max = " << max << endl;
}
