#include <iostream>
#include <queue>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/signals2.hpp>

using namespace std;

class Worker
{
    typedef boost::function<void ()> CommandType;
    queue<CommandType> tasks_;

    //boost::function<void (int)> end_of_processing_;
    boost::signals2::signal<void (int)> end_of_processing_;
public:
    void register_task(CommandType cmd)
    {
        tasks_.push(cmd);
    }

    void register_callback(boost::function<void(int)> callback)
    {
        end_of_processing_.connect(callback);
    }

    void run()
    {
        int no_of_tasks = 0;

        while(!tasks_.empty())
        {
            CommandType cmd = tasks_.front();
            cmd(); // wykonanie zadania
            tasks_.pop();
            ++no_of_tasks;
        }


        // wywolanie sygnalu
        end_of_processing_(no_of_tasks);
    }
};

class Printer
{
public:
    void print(const string& text)
    {
        cout << "Print:" << text << endl;
    }

    void off()
    {
        cout << "Printer.off\n";
    }

    void on()
    {
        cout << "Printer.on\n";
    }
};


class Logger
{
public:
    void log(int data)
    {
        cout << "Logging: " << data << endl;
    }
};

void console_logger(const string& prefix, int data)
{
    cout << prefix << " - data: " << data << endl;
}


int main()
{
    Printer prn;

    Worker worker;
    Logger logger;

    worker.register_task(boost::bind(&Printer::on, &prn));
    worker.register_task(boost::bind(&Printer::print, &prn, "Tekst"));
    worker.register_task(boost::bind(&Printer::off, &prn));

    //...

    worker.register_callback(boost::bind(&Logger::log, &logger, _1));
    worker.register_callback(boost::bind(&console_logger, "No of tasks: ", _1));

    worker.run();

    return 0;
}

